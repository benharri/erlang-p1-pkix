Source: erlang-p1-pkix
Maintainer: Ejabberd Packaging Team <ejabberd@packages.debian.org>
Uploaders: Philipp Huebner <debalance@debian.org>
Section: libs
Priority: optional
Rules-Requires-Root: no
Build-Depends: ca-certificates,
               debhelper-compat (= 13),
               dh-rebar,
               erlang-base,
               erlang-crypto,
               erlang-eunit,
               erlang-syntax-tools
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/ejabberd-packaging-team/erlang-p1-pkix
Vcs-Git: https://salsa.debian.org/ejabberd-packaging-team/erlang-p1-pkix.git
Homepage: https://github.com/processone/pkix

Package: erlang-p1-pkix
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         erlang-base | ${erlang-abi:Depends},
         ${erlang:Depends},
         ca-certificates
Multi-Arch: allowed
Description: PKIX certificates management library for Erlang
 The idea of the library is to simplify certificates configuration in Erlang
 programs. Typically an Erlang program which needs certificates  (for HTTPS/
 MQTT/XMPP/etc) provides a bunch of options such as certfile,  chainfile,
 privkey, etc. The situation becomes even more complicated when a  server
 supports so called virtual domains because a program is typically  required to
 match a virtual domain with its certificate. If a user has plenty  of virtual
 domains it's quickly becoming a nightmare for them to configure all this.
 The complexity also leads to errors: a single configuration mistake and a
 program generates obscure log messages, unreadable Erlang tracebacks or,
 even worse, just silently ignores the errors.
 Fortunately, the large part of certificates configuration can be automated,
 reducing a user configuration to something as simple as:
 .
 certfiles:
   - /etc/letsencrypt/live/*/*.pem
 .
 The purpose of this library is to do this dirty job under the hood.
